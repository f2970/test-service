variables:
  MAVEN_CLI_OPTS: "--batch-mode -s ci-settings.xml"
  MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - m2/repository

stages:
  - verify
  - build
  - test
  - integration-test
  - publish-code
  - prepare-publish-artifact
  - publish-artifact
  - publish-chart
  - deploy

verify:
  image: maven:3-amazoncorretto-17
  stage: verify
  when: on_success
  script:
    - mvn $MAVEN_CLI_OPTS verify

build:
  image: maven:3-amazoncorretto-17
  stage: build
  when: on_success
  script:
    - mvn $MAVEN_CLI_OPTS package -DskipTests
  artifacts:
    paths:
      - command/target
      - consumer/target
      - query/target

test:
  image: maven:3-amazoncorretto-17
  stage: test
  when: on_success
  script:
    - mvn $MAVEN_CLI_OPTS test

integration-test:
  image: maven:3-amazoncorretto-17
  stage: integration-test
  when: on_success
  script:
    - mvn $MAVEN_CLI_OPTS test -Dtest-groups=integration

publish-maven-snapshot:
  image: maven:3-amazoncorretto-17
  stage: publish-code
  script:
    - mvn $MAVEN_CLI_OPTS deploy
  only:
    - develop

publish-maven-release:
  image: maven:3-amazoncorretto-17
  stage: publish-code
  script:
    - mvn $MAVEN_CLI_OPTS versions:set -DremoveSnapshot
    - mvn $MAVEN_CLI_OPTS deploy
  artifacts:
    paths:
      - pom.xml
  only:
    - main

fetch-version:
  image: maven:3-amazoncorretto-17
  stage: prepare-publish-artifact
  script:
    - VERSION=$(mvn --non-recursive help:evaluate -Dexpression=project.version -q -DforceStdout)
    - echo $VERSION > build.version
  artifacts:
    paths:
      - build.version
  only:
    - main

publish-docker-snapshot-query:
  stage: publish-artifact
  image: docker:stable
  services:
    - docker:19.03.12-dind
  script:
    - sed -i s/IMAGE_TAG/"$CI_COMMIT_SHA"/g charts/test-service/values.yaml
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg APPLICATION=query/target/query-*.jar -t $CI_REGISTRY/f2970/test-service/query:$CI_COMMIT_SHA ./
    - docker push $CI_REGISTRY/f2970/test-service/query:$CI_COMMIT_SHA
  only:
    - develop
  dependencies:
    - build

publish-docker-snapshot-consumer:
  stage: publish-artifact
  image: docker:stable
  services:
    - docker:19.03.12-dind
  script:
    - sed -i s/IMAGE_TAG/"$CI_COMMIT_SHA"/g charts/test-service/values.yaml
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg APPLICATION=consumer/target/consumer-*.jar -t $CI_REGISTRY/f2970/test-service/consumer:$CI_COMMIT_SHA ./
    - docker push $CI_REGISTRY/f2970/test-service/consumer:$CI_COMMIT_SHA
  only:
    - develop
  dependencies:
    - build

publish-docker-snapshot-command:
  stage: publish-artifact
  image: docker:stable
  services:
    - docker:19.03.12-dind
  script:
    - sed -i s/IMAGE_TAG/"$CI_COMMIT_SHA"/g charts/test-service/values.yaml
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg APPLICATION=command/target/command-*.jar -t $CI_REGISTRY/f2970/test-service/command:$CI_COMMIT_SHA ./
    - docker push $CI_REGISTRY/f2970/test-service/command:$CI_COMMIT_SHA
  only:
    - develop
  dependencies:
    - build

publish-docker-release-query:
  stage: publish-artifact
  image: docker:stable
  services:
    - docker:19.03.12-dind
  script:
    - VERSION=$(cat build.version)
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg APPLICATION=query/target/query-*.jar -t $CI_REGISTRY/f2970/test-service/query:${VERSION} ./
    - docker push $CI_REGISTRY/f2970/test-service/query:${VERSION}
  only:
    - main
  dependencies:
    - build
    - fetch-version

publish-docker-release-consumer:
  stage: publish-artifact
  image: docker:stable
  services:
    - docker:19.03.12-dind
  script:
    - VERSION=$(cat build.version)
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg APPLICATION=consumer/target/consumer-*.jar -t $CI_REGISTRY/f2970/test-service/consumer:${VERSION} ./
    - docker push $CI_REGISTRY/f2970/test-service/consumer:${VERSION}
  only:
    - main
  dependencies:
    - build
    - fetch-version

publish-docker-release-command:
  stage: publish-artifact
  image: docker:stable
  services:
    - docker:19.03.12-dind
  script:
    - VERSION=$(cat build.version)
    - sed -i s/IMAGE_TAG/"$VERSION"/g charts/test-service/values.yaml
    - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg APPLICATION=command/target/command-*.jar -t $CI_REGISTRY/f2970/test-service/command:${VERSION} ./
    - docker push $CI_REGISTRY/f2970/test-service/command:${VERSION}
  only:
    - main
  dependencies:
    - build
    - fetch-version

publish-helm-snapshot:
  stage: publish-chart
  image: dtzar/helm-kubectl
  script:
    - sed -i s/IMAGE_TAG/"$CI_COMMIT_SHA"/g charts/test-service/values.yaml
    - helm package charts/test-service/
    - helm repo add --username gitlab-ci-token --password ${CI_JOB_TOKEN} ${CI_PROJECT_NAME} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/stable
    - helm plugin install https://github.com/chartmuseum/helm-push
    - helm cm-push test-service-*.tgz test-service
  only:
    - develop

publish-helm-release:
  stage: publish-chart
  image: dtzar/helm-kubectl
  script:
    - VERSION=$(cat build.version)
    - sed -i s/IMAGE_TAG/"$VERSION"/g charts/test-service/values.yaml
    - helm package charts/test-service/
    - helm repo add --username gitlab-ci-token --password ${CI_JOB_TOKEN} ${CI_PROJECT_NAME} ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/stable
    - helm plugin install https://github.com/chartmuseum/helm-push
    - helm cm-push test-service-*.tgz test-service
  only:
    - main

deploy-development:
  stage: deploy
  trigger:
    project: f2970/kubernetes
    branch: develop
  only:
    - develop

deploy-testing:
  stage: deploy
  trigger:
    project: f2970/kubernetes
    branch: testing
  only:
    - main