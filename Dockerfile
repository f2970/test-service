# We have to use Amazon Corretto as OpenJDK doesn't support custom runtimes entirely
FROM amazoncorretto:17-alpine AS runtime

ARG APPLICATION

COPY ${APPLICATION} app.jar

# Use jdeps to register which dependencies & Java modules our application relies on
RUN jdeps -q \
    --ignore-missing-deps \
    --multi-release 17 \
    --print-module-deps \
    app.jar > modules.txt \
    && cat modules.txt

# Use jlink to create a custom Java runtime with minimal dependencies & Java modules
RUN jlink --verbose \
    --add-modules java.base,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument \
    --compress 2 \
    --strip-java-debug-attributes \
    --no-header-files \
    --no-man-pages \
    --output /output/runtime \
    --vm server \
    --add-modules $(cat modules.txt)

FROM alpine:3

WORKDIR /app
ENTRYPOINT ["runtime/bin/java", "-server", "-jar", "app.jar"]

COPY --from=runtime /output/runtime runtime
COPY --from=runtime app.jar app.jar
